{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/master";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  description = "nix utility library";

  outputs = { self, nixpkgs, flake-utils, ... }@inputs: {
    lib = import (./. + "/lib") { inherit (inputs) nixpkgs flake-utils; };
  };
}
