{ nixpkgs, flake-utils, ... }:
with nixpkgs.lib;
let
  lib = makeExtensible (
    self:
      let
        callLibs = file: import file { lib = self; inherit nixpkgs flake-utils; };

        misc = callLibs ./misc.nix;
      in
        {
          inherit (misc) eachSystemMap eachDefaultSystemMap pkgsForSystem pkgsForSystem'
            mkReplaceModule flattenTreeWithSep
            ;
        }
  );
in
lib
